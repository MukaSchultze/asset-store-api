﻿using System;
using AssetStoreAPI.Data;

namespace AssetStoreAPI {
    public delegate void DataReceivedCallback<T>(RequestData<T> data) where T : JsonRequestObject;

    public static class AssetStore {

        public static void UserOverview(DataReceivedCallback<UserOverviewData> callback) {
            var url = Constants.USER_OVERVIEW_JSON_URL;
            var request = new AssetStoreRequest<UserOverviewData>(url);

            request.Send(callback);
        }

        public static void PublisherOverview(DataReceivedCallback<PublisherOverviewData> callback) {
            var url = Constants.PUBLISHER_OVERVIEW_JSON_URL;
            var request = new AssetStoreRequest<PublisherOverviewData>(url);

            request.Send(callback);
        }

        public static void GetSalesPeriods(int publisherID, DataReceivedCallback<SalesPeriodsData> callback) {
            var url = string.Format(Constants.SALES_PERIODS_JSON_URL, publisherID);
            var request = new AssetStoreRequest<SalesPeriodsData>(url);

            request.Send(callback);
        }

        public static void GetSales(int publisherID, int year, int month, DataReceivedCallback<AAContainer> callback) {
            if(month > 12 || month < 1)
                throw new ArgumentException("Invalid month");

            var url = string.Format(Constants.SALES_JSON_URL, publisherID, year, month);
            var request = new AssetStoreRequest<AAContainer>(url);

            request.Send(callback);
        }

        public static void GetSales(int publisherID, SalesPeriodsData.Period period, DataReceivedCallback<AAContainer> callback) {
            GetSales(publisherID, period.Year, period.Month, callback);
        }

        public static void GetFreeDownloads(int publisherID, SalesPeriodsData.Period period, DataReceivedCallback<AAContainer> callback) {
            GetFreeDownloads(publisherID, period.Year, period.Month, callback);
        }

        public static void GetFreeDownloads(int publisherID, int year, int month, DataReceivedCallback<AAContainer> callback) {
            if(month > 12 || month < 1)
                throw new ArgumentException("Invalid month");

            var url = string.Format(Constants.DOWNLOADS_JSON_URL, publisherID, year, month);
            var request = new AssetStoreRequest<AAContainer>(url);

            request.Send(callback);
        }

        public static void VerifyInvoice(int publisherID, int invoice, DataReceivedCallback<AAContainer> callback) {
            var url = string.Format(Constants.INVOICE_VERIFY_JSON_URL, publisherID, invoice);
            var request = new AssetStoreRequest<AAContainer>(url);

            request.Send(callback);
        }

        public static void GetRevenue(int publisherID, DataReceivedCallback<AAContainer> callback) {
            var url = string.Format(Constants.REVENUE_JSON_URL, publisherID);
            var request = new AssetStoreRequest<AAContainer>(url);

            request.Send(callback);
        }

        public static void GetPackages(DataReceivedCallback<PackagesData> callback) {
            var url = Constants.PACKAGES_JSON_URL;
            var request = new AssetStoreRequest<PackagesData>(url);

            request.Send(callback);
        }

        public static void APIKey(int publisherID, DataReceivedCallback<APIKeyData> callback) {
            var url = string.Format(Constants.API_KEY_JSON_URL, publisherID);
            var request = new AssetStoreRequest<APIKeyData>(url);

            request.Send(callback);
        }

    }
}