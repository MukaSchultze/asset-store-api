﻿using System;
using System.Net;

namespace AssetStoreAPI {
    public class AssetStoreException : Exception {
        public AssetStoreException(string message) : base(message) { }

        public AssetStoreException(HttpStatusCode httpCode, string message)
            : base(string.Format("Response code {0} {1}: {2}", (int)httpCode, httpCode, message)) { }

    }
}