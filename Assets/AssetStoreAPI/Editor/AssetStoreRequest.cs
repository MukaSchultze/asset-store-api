﻿using System;
using System.Net;
using AssetStoreAPI.Data;
using UnityEngine.Networking;

namespace AssetStoreAPI {
    public sealed class AssetStoreRequest<T> where T : JsonRequestObject {

        public string Url { get; private set; }
        public string Token { get; private set; }

        public AssetStoreRequest(string url, string token = null) {
            Url = url;
            Token = token;

            if(string.IsNullOrEmpty(Token))
                Token = Authentication.Token;
        }

        public void Send(DataReceivedCallback<T> callback) {
            var www = UnityWebRequest.Get(Url);

            if(!string.IsNullOrEmpty(Token)) {
                var cookies = new CookieContainer(1);
                var tokenCookie = new Cookie(Constants.TOKEN_COOKIE_NAME, Token);

#if UNITY_2018_1_OR_NEWER
                var uri = www.uri;
#else
                var uri = new Uri(www.url);
#endif

                cookies.Add(uri, tokenCookie);
                var cookiesHeader = cookies.GetCookieHeader(uri);

                www.SetRequestHeader("Cookie", cookiesHeader);
            }

            www.SendWebRequest().completed += op => {
                using(www) {
                    var obj = new RequestData<T>(www);
                    callback(obj);
                }
            };
        }

    }
}