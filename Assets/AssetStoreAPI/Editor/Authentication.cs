﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;
using AssetStoreAPI.Data;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace AssetStoreAPI {
    public static class Authentication {

        public static string Email {
            set { EditorPrefs.SetString("AssetStoreAPIEmail", value); }
            get { return EditorPrefs.GetString("AssetStoreAPIEmail"); }
        }

        public static string Password {
            set { EditorPrefs.SetString("AssetStoreAPIPassword", value); }
            get { return EditorPrefs.GetString("AssetStoreAPIPassword"); }
        }

        public static string Token {
            set { EditorPrefs.SetString("AssetStoreAPIToken", value); }
            get { return EditorPrefs.GetString("AssetStoreAPIToken"); }
        }

        public static void GetLoginToken(Action<string> onLoginSucess, Action<string> onLoginError) {
            GetLoginToken(Email, Password, onLoginSucess, onLoginError);
        }

        public static void GetLoginToken(string email, string password, Action<string> onLoginSucess, Action<string> onLoginError) {
            const string GENESIS_AUTH_COOKIE_NAME = "_genesis_auth_frontend_session";

            var request1 = UnityWebRequest.Get(Constants.SALES_URL);

            request1.SendWebRequest().completed += op1 => {
                using(request1)
                    try {
                        LogRequestError(request1, onLoginError);

                        var result1 = request1.downloadHandler.text;
                        var headers1 = request1.GetResponseHeaders();

                        var setCookieHeader = GetHeaderFromHeaderArray(headers1, "Set-Cookie", GENESIS_AUTH_COOKIE_NAME);
                        var authenticityTokenMatches = Regex.Matches(result1, "<input type=\"hidden\" name=\"authenticity_token\" value=\"(.+)\" />");

                        if(authenticityTokenMatches.Count == 0 || !authenticityTokenMatches[0].Success)
                            throw new AssetStoreException("Page authenticity token not found");

                        var authenticityToken = authenticityTokenMatches[0].Groups[1].Value;
                        var query = new Dictionary<string, string>() {
                            {"utf8", "✓"},
                            {"_method", "put"},
                            {"authenticity_token", authenticityToken },
                            {"conversations_create_session_form[email]", email},
                            {"conversations_create_session_form[password]", password},
                            {"conversations_create_session_form[remember_me]", "true"},
                            {"commit", "Log in"}
                        };

                        var request2 = UnityWebRequest.Post(request1.url, query);

                        request2.chunkedTransfer = false;
                        request2.SetRequestHeader("Cookie", setCookieHeader.Value);

                        request2.SendWebRequest().completed += op2 => {
                            using(request2)
                                try {
                                    LogRequestError(request2, onLoginError);

                                    var result2 = request2.downloadHandler.text;
                                    var bouncePageMatches = Regex.Matches(result2, "window\\.location\\.href \\= \\\"(.+)\\\"");

                                    if(bouncePageMatches.Count == 0 || !bouncePageMatches[0].Success)
                                        throw new AssetStoreException("Failed to get bounce page URL");

                                    var bouncePageURL = bouncePageMatches[0].Groups[1].Value;
                                    var request3 = UnityWebRequest.Get(bouncePageURL);

                                    request3.redirectLimit = 0;
                                    request3.SendWebRequest().completed += op3 => {
                                        using(request3)
                                            try {
                                                LogRequestError(request3, onLoginError);

                                                var headers3 = request3.GetResponseHeaders();


#if UNITY_2018_1_OR_NEWER
                                                var uri = request3.uri;
#else
                                                var uri = new Uri(request3.url);
#endif
                                                var kharmaSessionSetCookieHeader = GetHeaderFromHeaderArray(headers3, "Set-Cookie", Constants.TOKEN_COOKIE_NAME);
                                                var kharmaSessionParsedCookies = ParseCookies(uri, kharmaSessionSetCookieHeader.Value);
                                                var kharmaSession = UnityWebRequest.UnEscapeURL(kharmaSessionParsedCookies[Constants.TOKEN_COOKIE_NAME].Value);

                                                kharmaSession = kharmaSession.Trim();

                                                onLoginSucess(kharmaSession);
                                            }
                                            catch(Exception e) {
                                                Debug.LogException(e);
                                                onLoginError(e.Message);
                                            }
                                    };
                                }
                                catch(Exception e) {
                                    Debug.LogException(e);
                                    onLoginError(e.Message);
                                }
                        };
                    }
                    catch(Exception e) {
                        Debug.LogException(e);
                        onLoginError(e.Message);
                    }
            };
        }

        public static void Logout(DataReceivedCallback<JsonRequestObject> callback) {
            var url = Constants.LOGOUT_URL;
            var request = new AssetStoreRequest<JsonRequestObject>(url);

            request.Send(callback);
            Token = string.Empty;
            Password = string.Empty;
        }

        public static void ValidateToken(string token, Action<bool> result) {
            AssetStore.PublisherOverview(data => {
                result(data.ResponseCode == HttpStatusCode.OK);
            });
        }

        private static void LogRequestError(UnityWebRequest request, Action<string> onLoginError) {
            if(request.responseCode == 302)
                return;

            if(request.isHttpError)
                Debug.LogWarningFormat("Request response code: {0}", request.responseCode);
            if(!string.IsNullOrEmpty(request.error)) {
                Debug.LogError(request.error);
                onLoginError(request.error);
            }
        }

        private static CookieCollection ParseCookies(Uri uri, string header) {
            var cookies = new CookieContainer();
            cookies.SetCookies(uri, header);
            return cookies.GetCookies(uri);
        }

        private static KeyValuePair<string, string> GetHeaderFromHeaderArray(Dictionary<string, string> cookieArray, string cookieKey, string cookieValueStart = null) {
            foreach(var kvp in cookieArray)
                if(kvp.Key == cookieKey && (cookieValueStart == null || kvp.Value.Contains(cookieValueStart)))
                    return kvp;

            throw new KeyNotFoundException();
        }

    }
}