﻿namespace AssetStoreAPI {
    public static class Constants {

        public const string TOKEN_COOKIE_NAME = "kharma_session";

        public const string LOGOUT_URL = "https://publisher.assetstore.unity3d.com/logout";
        public const string SALES_URL = "https://publisher.assetstore.unity3d.com/sales.html";
        public const string USER_OVERVIEW_JSON_URL = "https://publisher.assetstore.unity3d.com/api/user/overview.json";
        public const string PUBLISHER_OVERVIEW_JSON_URL = "https://publisher.assetstore.unity3d.com/api/publisher/overview.json";
        public const string SALES_PERIODS_JSON_URL = "https://publisher.assetstore.unity3d.com/api/publisher-info/months/{0}.json";
        public const string SALES_JSON_URL = "https://publisher.assetstore.unity3d.com/api/publisher-info/sales/{0}/{1:0000}{2:00}.json";
        public const string DOWNLOADS_JSON_URL = "https://publisher.assetstore.unity3d.com/api/publisher-info/downloads/{0}/{1:0000}{2:00}.json";
        public const string INVOICE_VERIFY_JSON_URL = "https://publisher.assetstore.unity3d.com/api/publisher-info/verify-invoice/{0}/{1}.json";
        public const string REVENUE_JSON_URL = "https://publisher.assetstore.unity3d.com/api/publisher-info/revenue/{0}.json";
        public const string PACKAGES_JSON_URL = "https://publisher.assetstore.unity3d.com/api/management/packages.json";
        public const string API_KEY_JSON_URL = "https://publisher.assetstore.unity3d.com/api/publisher-info/api-key/{0}.json";

    }
}