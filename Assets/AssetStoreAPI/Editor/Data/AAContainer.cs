﻿using System;
using UnityEngine;

namespace AssetStoreAPI.Data {
    [Serializable]
    public class AAContainer : JsonRequestObject {
        [Serializable]
        public struct ExtraData {
            public string short_url;
        }

        [SerializeField]
        private string[] aaData;
        public ExtraData[] result;

        public SaleInfo[] SaleData { get { return SaleInfo.GetData(aaData, result); } }
        public DownloadInfo[] DownloadData { get { return DownloadInfo.GetData(aaData); } }
        public InvoiceInfo[] InvoiceData { get { return InvoiceInfo.GetData(aaData); } }
        public RevenueInfo[] RevenueData { get { return RevenueInfo.GetData(aaData); } }

    }
}