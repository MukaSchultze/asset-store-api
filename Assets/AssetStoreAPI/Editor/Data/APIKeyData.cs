﻿using System;

namespace AssetStoreAPI.Data {
    [Serializable]
    public class APIKeyData : JsonRequestObject {

        public string api_key;

    }
}