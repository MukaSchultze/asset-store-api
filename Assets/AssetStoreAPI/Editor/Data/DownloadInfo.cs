﻿using System;

namespace AssetStoreAPI.Data {
    [Serializable]
    public struct DownloadInfo {
        public string name;
        public int downloads;
        public string firstDownload;
        public string lastDownload;

        public static DownloadInfo[] GetData(string[] data) {
            var result = new DownloadInfo[data.Length / 4];

            for(var i = 0; i < result.Length; i++) {
                result[i].name = data[i * 4 + 0];
                result[i].downloads = int.Parse(data[i * 4 + 1]);
                result[i].firstDownload = data[i * 4 + 2];
                result[i].lastDownload = data[i * 4 + 3];
            }

            return result;
        }
    }
}