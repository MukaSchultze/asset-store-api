﻿using System;

namespace AssetStoreAPI.Data {
    [Serializable]
    public struct InvoiceInfo {
        public int number;
        public string name;
        public int quantity;
        public Price price;
        public string purchaseDate;
        public string status;

        public static InvoiceInfo[] GetData(string[] data) {
            var result = new InvoiceInfo[data.Length / 6];

            for(var i = 0; i < result.Length; i++) {
                result[i].number = int.Parse(data[i * 6 + 0]);
                result[i].name = data[i * 6 + 1];
                result[i].quantity = int.Parse(data[i * 6 + 2]);
                result[i].price = data[i * 6 + 3];
                result[i].purchaseDate = data[i * 6 + 4];
                result[i].status = data[i * 6 + 5];
            }

            return result;
        }
    }
}