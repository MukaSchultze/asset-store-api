﻿using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Networking;

namespace AssetStoreAPI.Data {
    //TODO: Implement chaching
    public class KeyImage {

        public string Url { get; private set; }
        public bool HasTexture { get; private set; }
        public bool Loading { get; private set; }
        public Texture2D Texture { get; private set; }

        public KeyImage(string url) {
            Url = url;
            Texture = Texture2D.whiteTexture;

            if(string.IsNullOrEmpty(Url))
                return;

            var www = UnityWebRequestTexture.GetTexture(url);

            Loading = true;
            www.SendWebRequest().completed += op => {
                using(www) {
                    if(string.IsNullOrEmpty(www.error))
                        Texture = DownloadHandlerTexture.GetContent(www);
                    else
                        Debug.LogErrorFormat("Failed to download key image at {0}:\n{1}", url, www.error);

                    Loading = false;
                    HasTexture = Texture != Texture2D.whiteTexture;
                    InternalEditorUtility.RepaintAllViews();
                }
            };
        }

        public static implicit operator Texture2D(KeyImage image) {
            return image.Texture;
        }

    }
}