﻿using System;

namespace AssetStoreAPI.Data {
    [Serializable]
    public class PackagesData : JsonRequestObject {

        [Serializable]
        public struct Version {
            public string status;
            public string name;
            public int package_id;
            public string modified;
            public int size;
            public string created;
            public string published;
            public string version_name;
            public int category_id;
            public int package_version_id;
            public string publishnotes;
            public int id;
            public string price;

            public Price Price { get { return price; } }
        }

        [Serializable]
        public struct Package {
            public Version[] versions;
            public int category_id;
            public string management_flags;
            public string name;
            public int id;
            public string short_url;
        }

        public Package[] packages = new Package[0];
        public string publisher_name;
        public int terms_current;
        public int terms_accepted;
        public int publisher_id;

    }
}