﻿using System;
using System.Globalization;

namespace AssetStoreAPI.Data {
    [Serializable]
    public struct Price {

        public string stringValue;
        public float numericValue;

        public Price(string str) {
            stringValue = str.Replace(" ", "");
            numericValue = float.Parse(stringValue, NumberStyles.Currency);
        }

        public static implicit operator Price(string str) {
            return new Price(str);
        }

        public override string ToString() {
            return stringValue;
        }

    }
}