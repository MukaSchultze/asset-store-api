﻿using System;
using UnityEngine;

namespace AssetStoreAPI.Data {
    [Serializable]
    public class PublisherOverviewData : JsonRequestObject {

        [Serializable]
        public struct Kategory {
            public string name;
            public string slug;
            public int id;
        }

        [Serializable]
        public struct Category {
            public string label_english;
            public string multiple;
            public int id;
            public string label;
        }

        [Serializable]
        public struct Publisher {
            public string label_english;
            public string slug;
            public string url;
            public int id;
            public string label;
            public string support_email;
            public string support_url;
        }

        [Serializable]
        public struct List {
            public string name;
            public string slug_v2;
            public string slug;
            public string overlay; //TODO: This might be a image, test it
        }

        [Serializable]
        public struct Link {
            public int id;
            public string type;
        }

        [Serializable]
        public struct Flags {
        }

        [Serializable]
        public struct Keyimage {
            [SerializeField]
            private string icon;
            [SerializeField]
            private string icon75;
            [SerializeField]
            private string icon25;

            private KeyImage m_icon;
            private KeyImage m_icon75;
            private KeyImage m_icon25;

            public KeyImage Icon { get { return m_icon ?? (m_icon = new KeyImage(icon)); } }
            public KeyImage Icon75 { get { return m_icon75 ?? (m_icon75 = new KeyImage(icon75)); } }
            public KeyImage Icon25 { get { return m_icon25 ?? (m_icon25 = new KeyImage(icon25)); } }
        }

        [Serializable]
        public struct Latest {
            [SerializeField]
            private string icon;
            public string pubdate;
            public string status;
            public Kategory kategory;
            public string package_version_id;
            public string slug;
            public int id;
            public Category category;
            public Publisher publisher;
            public List[] list;
            public Link link;
            public Flags flags;
            public string version;
            public Keyimage keyimage;
            public int license;
            public string title_english;
            public string title;

            private KeyImage m_icon;

            public KeyImage Icon { get { return m_icon ?? (m_icon = new KeyImage(icon)); } }
        }

        [Serializable]
        public struct Keyimage2 {
            [SerializeField]
            private string small;
            [SerializeField]
            private string big;

            private KeyImage m_small;
            private KeyImage m_big;

            public KeyImage Small { get { return m_small ?? (m_small = new KeyImage(small)); } }
            public KeyImage Big { get { return m_big ?? (m_big = new KeyImage(big)); } }
        }

        [Serializable]
        public struct Rating {
            public int count;
            public int average;
        }

        [Serializable]
        public struct Overview {
            public Latest latest;
            public bool services;
            public string name;
            public Keyimage2 keyimage;
            public string support_email;
            public string description;
            public Rating rating;
            public float payout_cut;
            public string url;
            public int id;
            public string short_url;
            public object support_url;
        }

        public Overview overview;

        public int PublisherID {
            get { return overview.id; }
        }

    }
}