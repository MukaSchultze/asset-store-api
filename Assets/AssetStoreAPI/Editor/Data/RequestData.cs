﻿using System.Net;
using UnityEngine;
using UnityEngine.Networking;

namespace AssetStoreAPI.Data {
    public sealed class RequestData<T> where T : JsonRequestObject {

        public T Object { get; private set; }
        public string Text { get; private set; }
        public string Error { get; private set; }
        public bool IsJSON { get; private set; }
        public HttpStatusCode ResponseCode { get; private set; }
        public UnityWebRequest Request { get; private set; }

        public RequestData(UnityWebRequest webRequest) {
            Request = webRequest;
            Text = webRequest.downloadHandler.text;
            ResponseCode = (HttpStatusCode)webRequest.responseCode;
            Error = webRequest.error;

            try {
                Object = JsonUtility.FromJson<T>(Text);
                IsJSON = true;
            }
            catch {
                IsJSON = false;
            }

            //HACK: Some requests returns nested arrays, this fix joins them into a single array.
            if(IsJSON && Text.Contains("aaData")) {
                var arrayLength = -1;
                var lastIndex = 0;

                while(lastIndex != -1 && arrayLength < 250) {
                    arrayLength++;
                    lastIndex = Text.IndexOf("],[", ++lastIndex);
                }

                if(arrayLength == 250)
                    throw new AssetStoreException("Failed to fix nested JSON array");

                Text = Text.Replace("],[", ", ");
                Text = Text.Replace("[[", "[");
                Text = Text.Replace("]]", "]");

                Object = JsonUtility.FromJson<T>(Text);
            }
        }

        public void AssertError() {
            if(!string.IsNullOrEmpty(Error))
                throw new AssetStoreException(ResponseCode, Error);
        }

    }
}