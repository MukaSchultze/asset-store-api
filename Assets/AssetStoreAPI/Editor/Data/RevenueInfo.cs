﻿using System;

namespace AssetStoreAPI.Data {
    [Serializable]
    public struct RevenueInfo {
        public string date;
        public string description;
        public Price debit;
        public Price credit;
        public Price balance;

        public static RevenueInfo[] GetData(string[] data) {
            var result = new RevenueInfo[data.Length / 5];

            for(var i = 0; i < result.Length; i++) {
                result[i].date = data[i * 5 + 0];
                result[i].description = data[i * 5 + 1];
                result[i].debit = data[i * 5 + 2];
                result[i].credit = data[i * 5 + 3];
                result[i].balance = data[i * 5 + 4];
            }

            return result;
        }
    }
}