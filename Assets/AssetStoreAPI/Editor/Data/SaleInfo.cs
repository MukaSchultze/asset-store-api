﻿using System;

namespace AssetStoreAPI.Data {
    [Serializable]
    public struct SaleInfo {
        public string name;
        public Price price;
        public int sells;
        public int refunds;
        public int chargebacks;
        public Price gross;
        public string firstSell;
        public string lastSell;
        public string short_url;

        public static SaleInfo[] GetData(string[] data, AAContainer.ExtraData[] extraData) {
            var result = new SaleInfo[data.Length / 8];

            for(var i = 0; i < result.Length; i++) {
                result[i].name = data[i * 8 + 0];
                result[i].price = data[i * 8 + 1];
                result[i].sells = int.Parse(data[i * 8 + 2]);
                result[i].refunds = int.Parse(data[i * 8 + 3]);
                result[i].chargebacks = int.Parse(data[i * 8 + 4]);
                result[i].gross = data[i * 8 + 5];
                result[i].firstSell = data[i * 8 + 6];
                result[i].lastSell = data[i * 8 + 7];
                result[i].short_url = extraData[i].short_url;
            }

            return result;
        }
    }
}