﻿using System;

namespace AssetStoreAPI.Data {
    [Serializable]
    public class SalesPeriodsData : JsonRequestObject {

        [Serializable]
        public struct Period {
            public int value;
            public string name;

            public int Month {
                get { return value % 100; }
                set { this.value = value * 100 + Year; }
            }
            public int Year {
                get { return value / 100; }
                set { this.value = Month * 100 + value; }
            }

        }

        public Period[] periods;

    }
}