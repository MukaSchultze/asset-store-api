﻿using System;
using UnityEngine;

namespace AssetStoreAPI.Data {
    [Serializable]
    public class UserOverviewData : JsonRequestObject {

        [Serializable]
        public struct Address {
            public bool lock_address_fields;
            public string country;
            public string firstname;
            public string country_name;
            public string lastname;
            public string phone;
            public string state;
            public string address2;
            public string email;
            public string zip;
            public string city;
            public string address;
            public string vat_no;
            public string organization;
        }

        [Serializable]
        public struct Publisher {
            public string email;
            public int id;
            public string label;
        }

        [Serializable]
        public struct KeyImages {

            [SerializeField]
            public string large;
            [SerializeField]
            private string small;
            [SerializeField]
            private string icon;
            [SerializeField]
            private string icon24;
            [SerializeField]
            private string medium;

            private KeyImage m_large;
            private KeyImage m_small;
            private KeyImage m_icon;
            private KeyImage m_icon24;
            private KeyImage m_medium;

            public KeyImage Large { get { return m_large ?? (m_large = new KeyImage(large)); } }
            public KeyImage Small { get { return m_small ?? (m_small = new KeyImage(small)); } }
            public KeyImage Icon { get { return m_icon ?? (m_icon = new KeyImage(icon)); } }
            public KeyImage Icon24 { get { return m_icon24 ?? (m_icon24 = new KeyImage(icon24)); } }
            public KeyImage Medium { get { return m_medium ?? (m_medium = new KeyImage(medium)); } }

        }

        [Serializable]
        public struct Balance {
            public string currency;
            public double amount;
            public string amount_text;
        }

        public string language_code;
        public string currency;
        public string email;
        public bool v2editor_allowed;
        public bool has_accepted_latest_terms;
        public bool himself;
        public int id;
        public Address address;
        public Publisher publisher;
        public string bio;
        public string name;
        public KeyImages keyimage;
        public Balance balance;
        public bool editable;
        public bool v2editor_preferred;
        public bool v2_preferred;

    }
}