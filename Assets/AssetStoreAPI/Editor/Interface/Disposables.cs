﻿using System;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace AssetStoreAPI.Interface {

    public sealed class GUIBackgroundColor : IDisposable {
        private readonly Color before;

        public GUIBackgroundColor(Color color) {
            before = GUI.backgroundColor;
            GUI.backgroundColor = color;
        }

        public void Dispose() {
            GUI.backgroundColor = before;
        }
    }

    public sealed class GUIContentColor : IDisposable {
        private readonly Color before;

        public GUIContentColor(Color color) {
            before = GUI.contentColor;
            GUI.contentColor = color;
        }

        public void Dispose() {
            GUI.contentColor = before;
        }
    }

    public sealed class GUIColor : IDisposable {
        private readonly Color before;

        public GUIColor(Color color) {
            before = GUI.color;
            GUI.color = color;
        }

        public void Dispose() {
            GUI.color = before;
        }
    }

    public sealed class GUIIndent : IDisposable {
        public GUIIndent() {
            EditorGUI.indentLevel++;
        }

        public GUIIndent(string label) {
            EditorGUILayout.LabelField(label);
            EditorGUI.indentLevel++;
        }

        public void Dispose() {
            EditorGUI.indentLevel--;
        }
    }

    public sealed class GUIEnabled : IDisposable {
        private readonly bool before;

        public GUIEnabled(bool enabled) {
            before = GUI.enabled;
            GUI.enabled = before && enabled;
        }

        public void Dispose() {
            GUI.enabled = before;
        }
    }

    public sealed class GUIMatrix : IDisposable {

        private readonly Matrix4x4 before;

        public GUIMatrix() : this(GUI.matrix) { }

        public GUIMatrix(Matrix4x4 matrix) {
            before = GUI.matrix;
            GUI.matrix = matrix;
        }

        public void Dispose() {
            GUI.matrix = before;
        }
    }

    public sealed class GUIFade : IDisposable {
        private AnimBool anim;

        public bool Visible { get; private set; }

        public GUIFade() {
            Visible = true;
        }

        public void SetTarget(bool target) {
            if(anim == null) {
                anim = new AnimBool(target);
                anim.valueChanged.AddListener(() => {
                    if(EditorWindow.focusedWindow)
                        EditorWindow.focusedWindow.Repaint();
                });
            }

            anim.target = target;
            Visible = EditorGUILayout.BeginFadeGroup(anim.faded);
        }

        public void Dispose() {
            EditorGUILayout.EndFadeGroup();
        }
    }

}