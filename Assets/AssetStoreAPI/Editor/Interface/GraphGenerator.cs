﻿using System;
using AssetStoreAPI.Data;
using UnityEditor.AnimatedValues;
using UnityEngine;
using UnityEngine.Events;

namespace AssetStoreAPI.Interface {

    public enum GraphMode {
        Sales,
        Gross,
        MinPrice,
        MaxPrice,
        Refunds,
        ChargeBacks,
    }

    [Serializable]
    public class GraphGenerator {

        public bool enabled;
        public string label;
        public GraphMode graphMode;
        public AnimFloat[] graph;
        public Color color;
        public PackagesData.Package package;

        [SerializeField]
        private AnimationCurve curve;

        public GraphGenerator(PackagesData.Package package, Color color) {
            this.package = package;
            label = package.name;
            enabled = package.versions[0].Price.numericValue > 0;
            this.color = color;
            SetSize(0);
        }

        public void SetSize(int size) {
            if(graph == null)
                graph = new AnimFloat[size];
            else
                Array.Resize(ref graph, size);

            for(var i = 0; i < size; i++)
                if(graph[i] == null)
                    graph[i] = new AnimFloat(0f);
                else {
                    graph[i].valueChanged = new UnityEvent();
                    graph[i].target = 0f;
                }

            curve = null;
        }

        public void UpdateData(int index, GraphMode mode, SaleInfo[] sales) {
            graphMode = mode;

            if(mode == GraphMode.MinPrice)
                graph[index].target = 10000f;

            foreach(var sale in sales) {
                if(sale.name != label)
                    continue;

                switch(graphMode) {
                    case GraphMode.Sales:
                        graph[index].target += sale.sells;
                        break;

                    case GraphMode.Gross:
                        graph[index].target += sale.gross.numericValue;
                        break;

                    case GraphMode.ChargeBacks:
                        graph[index].target += sale.chargebacks;
                        break;

                    case GraphMode.Refunds:
                        graph[index].target += sale.refunds;
                        break;

                    case GraphMode.MinPrice:
                        if(sale.price.numericValue > 0)
                            graph[index].target = Mathf.Min(graph[index].target, sale.price.numericValue);
                        break;

                    case GraphMode.MaxPrice:
                        graph[index].target = Mathf.Max(graph[index].target, sale.price.numericValue);
                        break;
                }
            }

            if(mode == GraphMode.MinPrice && graph[index].target == 10000f)
                graph[index].target = 0f;

        }

        public AnimationCurve GetAnimationCurve() {
            if(curve != null)
                return curve;

            curve = new AnimationCurve();

            for(var i = 0; i < graph.Length; i++) {
                var keyframe = new Keyframe() {
                    value = graph[i].value,
                    time = i,
                    inTangent = 0f,
                    outTangent = 0f,

#if UNITY_2018_1_OR_NEWER
                    weightedMode = WeightedMode.None,
                    inWeight = 0f,
                    outWeight = 0f,
#endif

                };

                var index = curve.AddKey(keyframe);

                if(graph[index].valueChanged == null)
                    graph[index].valueChanged = new UnityEvent();

                graph[index].valueChanged.AddListener(() => {
                    var keys = curve.keys;
                    keys[index].value = graph[index].value;
                    curve.keys = keys;
                });
            }

            return curve;
        }

    }
}