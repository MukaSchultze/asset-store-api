﻿using System;
using System.Collections.Generic;
using System.IO;
using AssetStoreAPI.Data;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AssetStoreAPI.Interface {
    public class GraphWindow : EditorWindow {

        private static class Styles {
            public static readonly GUIStyle graphLabel = "MiniBoldLabel";

            public static readonly GUIStyle toolbar = "toolbar";
            public static readonly GUIStyle toolbarDropdown = "ToolbarDropDown";
            public static readonly GUIStyle toolbarButton = "toolbarbutton";

            public static readonly GUIStyle oddRow = "OL EntryBackOdd";
            public static readonly GUIStyle evenRow = "OL EntryBackEven";
            public static readonly GUIStyle graphBar = "sequenceClip";

            public static readonly GUIStyle labelsBG = "RL Background";
            public static readonly GUIStyle labelsToggle = "OL Toggle";

            static Styles() {
                graphLabel = new GUIStyle(graphLabel);
                graphLabel.alignment = TextAnchor.MiddleCenter;
            }
        }

        [SerializeField]
        private bool barsEnabled = true;
        [SerializeField]
        private bool curveEnabled = false;
        [SerializeField]
        private bool labelsEnabled = true;
        [SerializeField]
        private GraphMode graphMode = GraphMode.Sales;
        [SerializeField]
        private SalesPeriodsData.Period from;
        [SerializeField]
        private SalesPeriodsData.Period to;

        [SerializeField]
        private float labelsHeight;
        [SerializeField]
        private Rect graphRect;
        [SerializeField]
        private SalesPeriodsData periodsData;
        [SerializeField]
        private SalesPeriodsData.Period fromOnRequest;
        [SerializeField]
        private SalesPeriodsData.Period toOnRequest;
        [SerializeField]
        private PackagesData packages;
        [SerializeField]
        private PublisherOverviewData publisher;
        [SerializeField]
        private List<GraphGenerator> graphs = new List<GraphGenerator>();
        [SerializeField]
        private List<Color> colors = new List<Color>();
        private List<GraphGenerator> enabledGraphs = new List<GraphGenerator>();
        private RequestCounter loadingData;

        [MenuItem("Asset Store API Tools/Graphs")]
        public static void Open() {
            var window = GetWindow<GraphWindow>();

            window.titleContent = new GUIContent("AS Graph");
        }

        private void OnEnable() {
            loadingData = new RequestCounter(this);
            LoadAllInfo();
        }

        private void Update() {
            if(loadingData)
                Repaint();
        }

        private void OnGUI() {
            using(new GUIEnabled(!loadingData)) {
                DrawToolbar();

                using(var scope = new EditorGUILayout.VerticalScope()) {
                    GUILayout.FlexibleSpace();

                    if(Event.current.type == EventType.Repaint)
                        graphRect = scope.rect;
                }

                enabledGraphs.Clear();

                for(var i = 0; i < graphs.Count; i++)
                    if(graphs[i].enabled)
                        enabledGraphs.Add(graphs[i]);

                DrawGraphs(enabledGraphs, graphRect);
                DrawGraphsLabel(graphs);
                DrawFooter();
            }
        }

        private void DrawToolbar() {
            using(new GUILayout.HorizontalScope(Styles.toolbar)) {

                EditorGUILayout.Space();

                if(GUILayout.Button("Save as PNG", Styles.toolbarButton, GUILayout.Width(100f)))
                    SaveToPng(graphRect);

                EditorGUILayout.Space();

                curveEnabled = GUILayout.Toggle(curveEnabled, "Curve", Styles.toolbarButton, GUILayout.Width(50f));
                barsEnabled = GUILayout.Toggle(barsEnabled, "Bars", Styles.toolbarButton, GUILayout.Width(50f));
                labelsEnabled = GUILayout.Toggle(labelsEnabled, "Labels", Styles.toolbarButton, GUILayout.Width(50f));

                GUILayout.FlexibleSpace();
                EditorGUI.BeginChangeCheck();

                using(new GUIBackgroundColor(to.value < from.value ? Color.red : Color.white)) {
                    EditorGUILayout.LabelField("From:", GUILayout.Width(38f));
                    if(EditorGUILayout.DropdownButton(new GUIContent(from.name), FocusType.Keyboard, Styles.toolbarDropdown, GUILayout.Width(105f)))
                        GetPeriodDropDown(from, selectedPeriod => from = selectedPeriod).ShowAsContext();

                    EditorGUILayout.Space();
                    EditorGUILayout.LabelField("To:", GUILayout.Width(25f));
                    if(EditorGUILayout.DropdownButton(new GUIContent(to.name), FocusType.Keyboard, Styles.toolbarDropdown, GUILayout.Width(105f)))
                        GetPeriodDropDown(to, selectedPeriod => to = selectedPeriod).ShowAsContext();
                }

                EditorGUILayout.Space();
                graphMode = (GraphMode)EditorGUILayout.EnumPopup(graphMode, Styles.toolbarDropdown, GUILayout.Width(90f));

                EditorGUILayout.Space();
                EditorGUILayout.Space();

                if(EditorGUI.EndChangeCheck())
                    LoadGraphs();

                using(new GUIEnabled(to.value >= from.value))
                    if(GUILayout.Button("Reload", Styles.toolbarButton, GUILayout.Width(65f)))
                        LoadGraphs();

                if(loadingData)
                    StatusWheel.DrawLayout(GUILayout.Width(15f));
            }
        }

        private void DrawFooter() {
            using(var footer = new EditorGUILayout.HorizontalScope(Styles.toolbar)) {
                var totalMonths = (toOnRequest.Year - fromOnRequest.Year) * 12 + (toOnRequest.Month - fromOnRequest.Month) + 1;
                var currentYear = fromOnRequest.Year;
                var currentMonth = fromOnRequest.Month;
                var index = 0;

                while(currentYear <= toOnRequest.Year) {
                    while((currentMonth <= toOnRequest.Month && currentYear == toOnRequest.Year) || (currentMonth <= 12 && currentYear < toOnRequest.Year))
                        using(new GUIMatrix()) {
                            var rect = footer.rect;

                            rect.xMin = footer.rect.xMin + footer.rect.width * ((float)index / totalMonths);
                            rect.xMax = footer.rect.xMin + footer.rect.width * ((float)(index + 1) / totalMonths);

                            if(rect.width > 60f)
                                EditorGUI.LabelField(rect, string.Format("{0:00}/{1:0000}", currentMonth, currentYear), Styles.toolbarButton);
                            else if(rect.width > 38f)
                                EditorGUI.LabelField(rect, string.Format("{0:00}/{1:00}", currentMonth, currentYear - 2000), Styles.toolbarButton);
                            else if(currentMonth == 1 || index == 0) {
                                var monthsInYear = 12;

                                if(currentYear == toOnRequest.Year)
                                    monthsInYear = toOnRequest.Month;

                                rect.xMax += (EditorGUIUtility.currentViewWidth / totalMonths) * (monthsInYear - currentMonth);
                                EditorGUI.LabelField(rect, string.Format("{0:0000}", currentYear), Styles.toolbarButton);
                            }

                            currentMonth++;
                            index++;
                        }
                    currentMonth = 1;
                    currentYear++;
                }

                GUILayout.FlexibleSpace();
            }
        }

        private void DrawGraphs(List<GraphGenerator> graphSettings, Rect rect) {
            var lowestValue = 0f;
            var highestValue = 15f;

            foreach(var graphSetting in graphSettings) {
                var graphData = graphSetting.graph;

                for(var i = 0; i < graphData.Length; i++) {
                    lowestValue = Mathf.Min(graphData[i].target, lowestValue);
                    highestValue = Mathf.Max(graphData[i].target * 1.2f, highestValue);
                }
            }

            if(Event.current.type == EventType.Repaint) {
                var rows = (highestValue - lowestValue);

                while(1f / rows * rect.height < 20f)
                    rows /= 2;

                for(var i = lowestValue; i <= rows; i++) {
                    var rowRect = rect;
                    var style = ((int)i & 1) == 0 ? Styles.oddRow : Styles.evenRow;

                    rowRect.yMin = rect.yMin + (1f - i / rows) * rect.height;
                    rowRect.yMax = rect.yMin + (1f - (i + 1) / rows) * rect.height;

                    rowRect.yMin = Mathf.Clamp(rowRect.yMin, rect.yMin + 5f, rect.yMax);
                    rowRect.yMax = Mathf.Clamp(rowRect.yMax, rect.yMin + 5f, rect.yMax);

                    style.Draw(rowRect, GUIContent.none, true, true, false, false);
                }
            }

            var index = 0;

            foreach(var graph in graphSettings) {
                var graphData = graph.graph;

                for(var i = 0; i < graphData.Length; i++) {
                    var horizontalPercent = (float)i / graphData.Length;
                    var verticalPercent = Mathf.InverseLerp(lowestValue, highestValue, graphData[i].value);
                    var horizontalPixels = (1f / graphData.Length) * rect.width / graphSettings.Count;
                    var graphRect = rect;

                    graphRect.xMin = rect.width * horizontalPercent + horizontalPixels * index;
                    graphRect.xMax = graphRect.xMin + horizontalPixels;
                    graphRect.yMin = rect.yMax - rect.height * verticalPercent;

                    graphRect.xMin += horizontalPixels * 0.05f;
                    graphRect.xMax -= horizontalPixels * 0.05f;

                    var labelRect = graphRect;

                    labelRect.yMin = labelRect.yMax = rect.height * (1f - verticalPercent);

                    labelRect.xMin -= 25f;
                    labelRect.xMax += 25f;
                    labelRect.yMin -= 15f;
                    labelRect.yMax += 30f;

                    graphRect.yMax += 25f; //Fix for overlapping bar texture

                    if(barsEnabled && Event.current.type == EventType.Repaint)
                        using(new GUIColor(graph.color))
                            Styles.graphBar.Draw(graphRect, GUIContent.none, -1);

                    if(labelsEnabled)
                        using(new GUIColor(barsEnabled ? Color.white : graph.color))
                            switch(graph.graphMode) {
                                case GraphMode.Gross:
                                case GraphMode.MinPrice:
                                case GraphMode.MaxPrice:
                                    EditorGUI.LabelField(labelRect, graphData[i].value.ToString("C"), Styles.graphLabel);
                                    break;

                                default:
                                    EditorGUI.LabelField(labelRect, graphData[i].value.ToString("0"), Styles.graphLabel);
                                    break;
                            }
                }

                if(curveEnabled) {
                    var curveRect = new Rect(-0.5f, 0f, graphData.Length, highestValue);
                    EditorGUIUtility.DrawCurveSwatch(rect, graph.GetAnimationCurve(), null, graph.color, Color.clear, curveRect);
                }

                index++;
            }
        }

        private void DrawGraphsLabel(List<GraphGenerator> graphSettings) {
            if(graphSettings.Count == 0)
                return;

            using(new GUILayout.AreaScope(new Rect(10f, 21f, 190f, labelsHeight), GUIContent.none, Styles.labelsBG))
            using(var labelsRect = new EditorGUILayout.VerticalScope()) {
                if(Event.current.type == EventType.Repaint)
                    labelsHeight = labelsRect.rect.height;

                EditorGUILayout.Space();

                foreach(var graph in graphSettings) {
                    if(graph.package.versions[0].Price.numericValue <= 0f)
                        continue;

                    using(new EditorGUILayout.HorizontalScope()) {
#if UNITY_2018_1_OR_NEWER
                        graph.color = EditorGUILayout.ColorField(GUIContent.none, graph.color, false, false, false, GUILayout.Width(10f));
#else
                        var colorRect = EditorGUILayout.GetControlRect(GUILayout.Width(10f));
                        EditorGUIUtility.DrawColorSwatch(colorRect, graph.color);
#endif
                        graph.enabled = EditorGUILayout.Toggle(graph.label, graph.enabled, Styles.labelsToggle);
                    }
                }

                EditorGUILayout.Space();
            }
        }

        private void LoadGraphs() {
            if(to.value < from.value)
                return;

            fromOnRequest = from;
            toOnRequest = to;

            var totalMonths = (to.Year - from.Year) * 12 + (to.Month - from.Month) + 1;
            var currentYear = from.Year;
            var currenMonth = from.Month;
            var index = 0;

            for(var i = 0; i < graphs.Count; i++) {
                graphs[i].SetSize(totalMonths);

                for(var j = 0; j < graphs[i].graph.Length; j++)
                    graphs[i].graph[j].valueChanged.AddListener(Repaint);
            }

            while(currentYear <= to.Year) {
                while((currenMonth <= to.Month && currentYear == to.Year) || (currenMonth <= 12 && currentYear < to.Year)) {
                    var localIndex = index++; //Works with lambda expressions.

                    loadingData.LoadingData = true;
                    AssetStore.GetSales(publisher.PublisherID, currentYear, currenMonth, data => {
                        loadingData.LoadingData = false;
                        data.AssertError();

                        var sales = data.Object.SaleData;

                        for(var i = 0; i < graphs.Count; i++)
                            graphs[i].UpdateData(localIndex, graphMode, sales);
                    });

                    currenMonth++;
                }
                currenMonth = 1;
                currentYear++;
            }

        }

        private void LoadAllInfo() {
            if(publisher == null)
                LoadPublisherInfo();
            if(packages == null)
                LoadPackagesInfo();
        }

        private void LoadPublisherInfo() {
            loadingData.LoadingData = true;
            AssetStore.PublisherOverview(data => {
                loadingData.LoadingData = false;
                data.AssertError();
                publisher = data.Object;
                LoadPeriodsInfo();
            });
        }

        private void LoadPeriodsInfo() {
            loadingData.LoadingData = true;
            AssetStore.GetSalesPeriods(publisher.PublisherID, periods => {
                loadingData.LoadingData = false;
                periods.AssertError();
                periodsData = periods.Object;

                if(periodsData != null && periodsData.periods.Length > 0)
                    from = to = periodsData.periods[0];
            });
        }

        private void LoadPackagesInfo() {
            loadingData.LoadingData = true;
            AssetStore.GetPackages(packagesData => {
                loadingData.LoadingData = false;
                graphs.Clear();
                packagesData.AssertError();
                packages = packagesData.Object;
                GenerateAssetColors(packages.packages.Length);

                for(var i = 0; i < packages.packages.Length; i++)
                    graphs.Add(new GraphGenerator(packages.packages[i], colors[i]));
            });
        }

        private void GenerateAssetColors(int count) {
            colors.Clear();

            for(var i = 0; i < count; i++) {
                var h = (float)i / count;
                var s = Mathf.Lerp(0.7f, 1f, Random.value);
                var v = Mathf.Lerp(0.7f, 1f, Random.value);

                colors.Add(Color.HSVToRGB(h, s, v));
            }
        }

        private GenericMenu GetPeriodDropDown(SalesPeriodsData.Period referencePeriod, Action<SalesPeriodsData.Period> selectedPeriod) {
            var result = new GenericMenu();

            if(periodsData != null && periodsData.periods != null)
                for(var i = 0; i < periodsData.periods.Length; i++) {
                    var period = periodsData.periods[i];
                    var content = new GUIContent(period.name);

                    result.AddItem(content, period.value == referencePeriod.value, () => {
                        selectedPeriod(period);
                        LoadGraphs();
                    });
                }

            return result;
        }

        private void SaveToPng(Rect rect) {
            var width = (int)rect.width;
            var heigth = (int)rect.height + 17;
            var colors = InternalEditorUtility.ReadScreenPixel(rect.position + position.position, width, heigth);
            var path = EditorUtility.SaveFilePanel("Save Graph", "", "graph", "png");

            if(string.IsNullOrEmpty(path))
                return;

            var texture = new Texture2D(width, heigth, TextureFormat.ARGB32, false, true);

            try {
                texture.SetPixels(colors);
                texture.Apply();

                var bytes = texture.EncodeToPNG();

                File.WriteAllBytes(path, bytes);
            }
            finally {
                DestroyImmediate(texture);
            }
        }

    }
}