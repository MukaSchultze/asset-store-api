﻿using AssetStoreAPI.Data;
using UnityEditor;
using UnityEngine;

namespace AssetStoreAPI.Interface {

    public enum LoginStatus {
        Logged,
        LoginError,
        NotLogged,
        Logging,
        LoggingOut
    }

    public class LoginWindow : EditorWindow {

        private static class Styles {
            public static readonly GUIStyle title = "HeaderLabel";
            public static readonly GUIStyle fieldLabel = "BoldLabel";
            public static readonly GUIStyle loginButton = "AC Button";

            static Styles() {
                title = new GUIStyle(title);
                title.alignment = TextAnchor.MiddleCenter;
                title.fontSize = 17;
            }
        }

        [SerializeField]
        private bool closeOnLogin;
        [SerializeField]
        private Vector2 scroll;
        [SerializeField]
        private UserOverviewData userData = new UserOverviewData();
        [SerializeField]
        private LoginStatus loginStatus = LoginStatus.NotLogged;
        private RequestCounter loadingData;

        private LoginStatus LoginStatus {
            get { return loginStatus; }
            set {
                loginStatus = value;
                Repaint();
            }
        }

        [MenuItem("Asset Store API Tools/Login")]
        public static void Open() { Open(false); }

        public static void Open(bool closeOnLogin) {
            var window = GetWindow<LoginWindow>(true);

            window.closeOnLogin = closeOnLogin;
            window.titleContent = new GUIContent("Asset Store Login");
            window.minSize = window.maxSize = new Vector2(260f, 260f);
        }

        private void Update() {
            if(loadingData)
                Repaint();

            if(closeOnLogin && LoginStatus == LoginStatus.Logged)
                Close();
        }

        private void OnEnable() {
            loadingData = new RequestCounter(this);
            ValidateToken();
        }

        private void OnGUI() {
            using(new EditorGUILayout.HorizontalScope()) {
                GUILayout.FlexibleSpace();
                EditorGUILayout.Space();

                using(new EditorGUILayout.VerticalScope()) {
                    using(new GUIEnabled(!loadingData))
                        switch(LoginStatus) {
                            case LoginStatus.NotLogged:
                            case LoginStatus.LoginError:
                            case LoginStatus.Logging:
                            case LoginStatus.LoggingOut:
                                LoginGUI();
                                break;

                            case LoginStatus.Logged:
                                LoggedGUI();
                                break;
                        }
                }

                EditorGUILayout.Space();
                GUILayout.FlexibleSpace();
            }

            if(loadingData)
                StatusWheel.Draw(new Rect(position.size * 0.5f - Vector2.one * 11f, Vector2.one * 22f));
        }

        private void LoggedGUI() {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField(userData.name, Styles.title, GUILayout.Height(25f));
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            using(new EditorGUILayout.HorizontalScope()) {
                using(new EditorGUILayout.VerticalScope()) {
                    EditorGUILayout.LabelField(userData.publisher.label, GUILayout.Width(180f));
                    EditorGUILayout.LabelField(userData.email, GUILayout.Width(180f));
                }

                EditorGUILayout.Space();
                var texture = Texture2D.whiteTexture;
                var rect = EditorGUILayout.GetControlRect(false, GUILayout.Height(40f), GUILayout.Width(40f));
                GUI.DrawTexture(rect, userData.keyimage.Medium, ScaleMode.ScaleToFit);
                EditorGUILayout.Space();
            }

            EditorGUILayout.Space();
            scroll = EditorGUILayout.BeginScrollView(scroll);
            EditorGUILayout.LabelField(userData.bio, EditorStyles.wordWrappedMiniLabel);
            EditorGUILayout.EndScrollView();

            GUILayout.FlexibleSpace();
            EditorGUILayout.Space();

            if(GUILayout.Button("Logout", Styles.loginButton, GUILayout.Height(25f)))
                StartLogout();

            EditorGUILayout.SelectableLabel(Authentication.Token, EditorStyles.centeredGreyMiniLabel);
        }

        private void LoginGUI() {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Asset Store Login", Styles.title, GUILayout.Height(25f));

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Email", Styles.fieldLabel);
            Authentication.Email = EditorGUILayout.TextField(Authentication.Email);

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Password", Styles.fieldLabel);
            Authentication.Password = EditorGUILayout.PasswordField(Authentication.Password);

            EditorGUILayout.Space();

            if(LoginStatus == LoginStatus.LoginError)
                EditorGUILayout.HelpBox("An error occurred, please re-check your email and password and try again", MessageType.Error);

            GUILayout.FlexibleSpace();
            EditorGUILayout.Space();

            using(new GUIEnabled(!string.IsNullOrEmpty(Authentication.Email) && !string.IsNullOrEmpty(Authentication.Password)))
                if(GUILayout.Button("Login", Styles.loginButton, GUILayout.Height(25f)))
                    StartLogin();

            if(!string.IsNullOrEmpty(Authentication.Email) && !string.IsNullOrEmpty(Authentication.Password))
                if(Event.current.type == EventType.KeyUp && (Event.current.keyCode == KeyCode.Return || Event.current.keyCode == KeyCode.KeypadEnter)) {
                    Event.current.Use();
                    StartLogin();
                }

            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }

        private void StartLogin() {
            LoginStatus = LoginStatus.Logging;
            loadingData.LoadingData = true;
            Authentication.GetLoginToken(LoginSucess, LoginError);
        }

        private void StartLogout() {
            LoginStatus = LoginStatus.LoggingOut;
            loadingData.LoadingData = true;

            Authentication.Logout(data => {
                loadingData.LoadingData = false;
                LoginStatus = LoginStatus.NotLogged;
                data.AssertError();
            });
        }

        private void LoginSucess(string token) {
            loadingData.LoadingData = false;
            LoginStatus = LoginStatus.Logged;
            Authentication.Token = token;
            ReloadUserData();
        }

        private void LoginError(string message) {
            loadingData.LoadingData = false;
            LoginStatus = LoginStatus.LoginError;
            Authentication.Token = string.Empty;
            Debug.LogError(message);
        }

        private void ValidateToken() {
            loadingData.LoadingData = true;
            Authentication.ValidateToken(Authentication.Token, data => {
                LoginStatus = data ? LoginStatus.Logged : LoginStatus.NotLogged;
                loadingData.LoadingData = false;

                if(data)
                    ReloadUserData();
                else
                    StartLogout();
            });
        }

        private void ReloadUserData() {
            loadingData.LoadingData = true;
            AssetStore.UserOverview(data => {
                loadingData.LoadingData = false;
                data.AssertError();
                if(data.Object != null)
                    userData = data.Object;
            });
        }
    }
}