﻿using UnityEditor;
using UnityEngine;

namespace AssetStoreAPI.Interface {
    public class RequestCounter {

        private int activeRequests;
        private EditorWindow window;

        public RequestCounter(EditorWindow window) {
            this.window = window;
        }

        public bool LoadingData {
            get { return activeRequests > 0; }
            set {
                if(value)
                    activeRequests++;
                else
                    activeRequests--;

                activeRequests = Mathf.Max(0, activeRequests);

                if(window)
                    window.Repaint();
            }
        }

        public static implicit operator bool(RequestCounter counter) {
            return counter.LoadingData;
        }

    }
}
