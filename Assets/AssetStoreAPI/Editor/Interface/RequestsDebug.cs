﻿using System;
using System.Net;
using AssetStoreAPI.Data;
using UnityEditor;
using UnityEngine;

namespace AssetStoreAPI.Interface {
    public class RequestsDebug : EditorWindow {

        private enum RequestType {
            Logout,
            UserOverview,
            PublisherOverview,
            SalesPeriods,
            Sales,
            FreeDownloads,
            VerifyInvoice,
            Revenue,
            Packages,
            APIKey,
        }

        [SerializeField]
        private int publisherID;
        [SerializeField]
        private int invoice;
        [SerializeField]
        private int month;
        [SerializeField]
        private int year;

        private RequestCounter requesting;
        [SerializeField]
        private Vector2 scroll;
        [SerializeField]
        private string requestText;
        [SerializeField]
        private string error;
        [SerializeField]
        private HttpStatusCode requestCode = 0;
        [SerializeField]
        private RequestType selectedRequest;

        [MenuItem("Asset Store API Tools/Debug")]
        public static void Open() {
            var window = GetWindow<RequestsDebug>();

            window.titleContent = new GUIContent("AS Debug");
        }

        private void OnEnable() {
            requesting = new RequestCounter(this);
        }

        private void Update() {
            if(requesting)
                Repaint();
        }

        private void OnGUI() {
            using(new GUIEnabled(!requesting)) {
                selectedRequest = (RequestType)EditorGUILayout.EnumPopup(selectedRequest);

                switch(selectedRequest) {
                    case RequestType.APIKey:
                    case RequestType.SalesPeriods:
                    case RequestType.Revenue:
                        publisherID = EditorGUILayout.IntField("PublisherID", publisherID);
                        break;

                    case RequestType.Sales:
                    case RequestType.FreeDownloads:
                        publisherID = EditorGUILayout.IntField("PublisherID", publisherID);
                        year = EditorGUILayout.IntField("Year", year);
                        month = EditorGUILayout.IntField("Month", month);
                        break;

                    case RequestType.VerifyInvoice:
                        publisherID = EditorGUILayout.IntField("PublisherID", publisherID);
                        invoice = EditorGUILayout.IntField("Invoice No", invoice);
                        break;
                }

                EditorGUILayout.Space();

                if(GUILayout.Button("Send Request"))
                    DoRequest();

                scroll = EditorGUILayout.BeginScrollView(scroll);

                if(!string.IsNullOrEmpty(error))
                    EditorGUILayout.HelpBox(error, MessageType.Error);
                if(requestCode != 0)
                    EditorGUILayout.HelpBox(string.Format("Request code {0}: {1}", (int)requestCode, requestCode), MessageType.Warning);
                if(!string.IsNullOrEmpty(requestText))
                    EditorGUILayout.HelpBox(requestText, MessageType.Info);

                EditorGUILayout.EndScrollView();

                if(requesting)
                    StatusWheel.DrawLayout();
            }
        }

        private void DoRequest() {
            switch(selectedRequest) {
                case RequestType.Logout:
                    Authentication.Logout(RequestCallback);
                    requesting.LoadingData = true;
                    break;

                case RequestType.UserOverview:
                    AssetStore.UserOverview(RequestCallback);
                    requesting.LoadingData = true;
                    break;

                case RequestType.PublisherOverview:
                    AssetStore.PublisherOverview(RequestCallback);
                    requesting.LoadingData = true;
                    break;

                case RequestType.SalesPeriods:
                    AssetStore.GetSalesPeriods(publisherID, RequestCallback);
                    requesting.LoadingData = true;
                    break;

                case RequestType.Sales:
                    AssetStore.GetSales(publisherID, year, month, RequestCallback);
                    requesting.LoadingData = true;
                    break;

                case RequestType.FreeDownloads:
                    AssetStore.GetFreeDownloads(publisherID, year, month, RequestCallback);
                    requesting.LoadingData = true;
                    break;

                case RequestType.VerifyInvoice:
                    AssetStore.VerifyInvoice(publisherID, invoice, RequestCallback);
                    requesting.LoadingData = true;
                    break;

                case RequestType.Revenue:
                    AssetStore.GetRevenue(publisherID, RequestCallback);
                    requesting.LoadingData = true;
                    break;

                case RequestType.Packages:
                    AssetStore.GetPackages(RequestCallback);
                    requesting.LoadingData = true;
                    break;

                case RequestType.APIKey:
                    AssetStore.APIKey(publisherID, RequestCallback);
                    requesting.LoadingData = true;
                    break;

                default:
                    throw new NotImplementedException();
            }
        }

        private void RequestCallback<T>(RequestData<T> data) where T : JsonRequestObject {
            requesting.LoadingData = false;
            error = data.Error;
            requestCode = data.ResponseCode;
            requestText = data.Text;

            Repaint();

            if(data.IsJSON)
                requestText = JsonUtility.ToJson(data.Object, true);
        }

    }
}