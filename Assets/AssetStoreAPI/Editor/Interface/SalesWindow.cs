﻿using System;
using AssetStoreAPI.Data;
using UnityEditor;
using UnityEngine;

namespace AssetStoreAPI.Interface {
    public class SalesWindow : EditorWindow {

        private static class Styles {
            public static readonly GUIStyle toolbar = "toolbar";
            public static readonly GUIStyle toolbarDropdown = "ToolbarDropDown";
            public static readonly GUIStyle toolbarButton = "toolbarbutton";
            public static readonly GUIStyle oddRow = "ObjectPickerResultsOdd";
            public static readonly GUIStyle evenRow = "ObjectPickerResultsEven";
            public static readonly GUIStyle verticalLine = "EyeDropperVerticalLine";
            public static readonly GUIStyle openAssetURL = "ProfilerTimelineFoldout";

            public static readonly GUIStyle sortToolbar = "toolbar";
            public static readonly GUIStyle sortButton = "toolbarbutton";
            public static readonly GUIStyle sortButtonSelected = "ToolbarDropDown";
        }

        [SerializeField]
        private Vector2 scroll;
        [SerializeField]
        private bool scrollIsVisible;
        [SerializeField]
        private PublisherOverviewData publisher;
        [SerializeField]
        private SalesPeriodsData periodsData;
        [SerializeField]
        private SalesPeriodsData.Period selectedPeriod;
        [SerializeField]
        private SaleInfo[] sales;
        [SerializeField]
        private SaleInfo selectedSale;
        private RequestCounter loadingData;

        private SalesPeriodsData.Period SelectedPeriod {
            get { return selectedPeriod; }
            set {
                loadingData.LoadingData = true;
                selectedPeriod = value;

                AssetStore.GetSales(publisher.PublisherID, value, data => {
                    loadingData.LoadingData = false;
                    data.AssertError();
                    sales = data.Object.SaleData;
                });
            }
        }

        private GenericMenu PeriodDropDown {
            get {
                var result = new GenericMenu();

                if(periodsData != null && periodsData.periods != null)
                    for(var i = 0; i < periodsData.periods.Length; i++) {
                        var period = periodsData.periods[i];
                        var content = new GUIContent(period.name);

                        result.AddItem(content, period.value == SelectedPeriod.value, () => {
                            SelectedPeriod = period;
                        });
                    }

                return result;
            }
        }

        [MenuItem("Asset Store API Tools/Sales")]
        public static void Open() {
            var window = GetWindow<SalesWindow>();

            window.titleContent = new GUIContent("AS Sales");
        }

        private void OnEnable() {
            wantsMouseMove = true;
            wantsMouseEnterLeaveWindow = true;
            loadingData = new RequestCounter(this);

            LoadPublisherInfo();
        }

        private void Update() {
            if(loadingData)
                Repaint();
        }

        private void OnGUI() {
            using(new GUIEnabled(!loadingData)) {
                DrawToolbar();
                DoSaleHeader();

                if(sales != null)
                    using(var rect1 = new EditorGUILayout.VerticalScope()) {
                        scroll = EditorGUILayout.BeginScrollView(scroll);
                        using(var rect2 = new EditorGUILayout.VerticalScope()) {
                            scrollIsVisible = rect1.rect.width > rect2.rect.width;

                            for(var i = 0; i < sales.Length; i++)
                                DoSaleRow(sales[i], (i & 1) == 0);

                            EditorGUILayout.EndScrollView();
                        }
                    }
            }
        }

        private void DrawToolbar() {
            using(new GUILayout.HorizontalScope(Styles.toolbar)) {

                if(publisher != null && sales != null) {
                    var grossValue = 0f;

                    for(var i = 0; i < sales.Length; i++)
                        grossValue += sales[i].gross.numericValue;

                    var gross = string.Format("Gross: ${0:0.00}", grossValue);
                    var net = string.Format("Net: ${0:0.00} ({1:00%})", grossValue * publisher.overview.payout_cut, publisher.overview.payout_cut);

                    GUILayout.FlexibleSpace();
                    EditorGUILayout.LabelField(gross, EditorStyles.boldLabel, GUILayout.Width(150f));
                    EditorGUILayout.LabelField(net, EditorStyles.boldLabel, GUILayout.Width(150f));
                }

                GUILayout.FlexibleSpace();

                if(EditorGUILayout.DropdownButton(new GUIContent(SelectedPeriod.name), FocusType.Keyboard, Styles.toolbarDropdown, GUILayout.Width(105f)))
                    PeriodDropDown.ShowAsContext();

                if(loadingData)
                    StatusWheel.DrawLayout(GUILayout.Width(15f));
            }
        }

        private void DoSaleHeader() {
            using(new GUILayout.HorizontalScope(Styles.sortToolbar)) {

                if(GUILayout.Button("Name", Styles.sortButton, GUILayout.MinWidth(224f), GUILayout.ExpandWidth(true)))
                    if(sales != null)
                        Array.Sort(sales, (a, b) => { return string.Compare(b.name, a.name); });

                DrawSeparator();
                if(GUILayout.Button("Price", Styles.sortButton, GUILayout.Width(65f)))
                    if(sales != null)
                        Array.Sort(sales, (a, b) => { return b.price.numericValue.CompareTo(a.price.numericValue); });

                DrawSeparator();
                if(GUILayout.Button("Qty", Styles.sortButton, GUILayout.Width(45f)))
                    if(sales != null)
                        Array.Sort(sales, (a, b) => { return b.sells.CompareTo(a.sells); });

                DrawSeparator();
                if(GUILayout.Button("Refunds", Styles.sortButton, GUILayout.Width(65f)))
                    if(sales != null)
                        Array.Sort(sales, (a, b) => { return b.refunds.CompareTo(a.refunds); });

                DrawSeparator();
                if(GUILayout.Button("Chrg backs", Styles.sortButton, GUILayout.Width(65f)))
                    if(sales != null)
                        Array.Sort(sales, (a, b) => { return b.chargebacks.CompareTo(a.chargebacks); });

                DrawSeparator();
                if(GUILayout.Button("Gross", Styles.sortButton, GUILayout.Width(65f)))
                    if(sales != null)
                        Array.Sort(sales, (a, b) => { return b.gross.numericValue.CompareTo(a.gross.numericValue); });

                DrawSeparator();
                if(GUILayout.Button("First", Styles.sortButton, GUILayout.Width(65f)))
                    if(sales != null)
                        Array.Sort(sales, (a, b) => { return string.Compare(b.firstSell, a.firstSell); });

                DrawSeparator();
                if(GUILayout.Button("Last", Styles.sortButton, GUILayout.Width(65f)))
                    if(sales != null)
                        Array.Sort(sales, (a, b) => { return string.Compare(b.lastSell, a.lastSell); });

                if(scrollIsVisible)
                    GUILayout.Space(15f);
            }
        }

        private void DoSaleRow(SaleInfo sale, bool odd) {
            var style = odd ? Styles.oddRow : Styles.evenRow;

            using(var scope = new EditorGUILayout.HorizontalScope(style)) {
                var isHover = scope.rect.Contains(Event.current.mousePosition);

                if(Event.current.type == EventType.Repaint)
                    style.Draw(scope.rect, GUIContent.none, sale.Equals(selectedSale), true, isHover, false);

                EditorGUILayout.LabelField(sale.name, GUILayout.ExpandWidth(true));

                if(GUILayout.Button(string.Empty, Styles.openAssetURL, GUILayout.Width(15f), GUILayout.Height(20f)))
                    Application.OpenURL(sale.short_url);

                DrawSeparator();
                EditorGUILayout.LabelField(sale.price.stringValue, GUILayout.Width(65f));
                DrawSeparator();
                EditorGUILayout.LabelField(sale.sells.ToString(), GUILayout.Width(45f));
                DrawSeparator();
                EditorGUILayout.LabelField(sale.refunds.ToString(), GUILayout.Width(65f));
                DrawSeparator();
                EditorGUILayout.LabelField(sale.chargebacks.ToString(), GUILayout.Width(65f));
                DrawSeparator();
                EditorGUILayout.LabelField(sale.gross.stringValue, GUILayout.Width(65f));
                DrawSeparator();
                EditorGUILayout.LabelField(sale.firstSell, GUILayout.Width(65f));
                DrawSeparator();
                EditorGUILayout.LabelField(sale.lastSell, GUILayout.Width(65f));

                switch(Event.current.type) {
                    case EventType.MouseMove:
                    case EventType.MouseEnterWindow:
                    case EventType.MouseLeaveWindow:
                        Repaint();
                        break;

                    case EventType.MouseUp:
                        if(isHover) {
                            if(sale.Equals(selectedSale))
                                selectedSale = new SaleInfo();
                            else
                                selectedSale = sale;
                            Event.current.Use();
                        }
                        break;
                }
            }
        }

        private void DrawSeparator() {
            var rect = EditorGUILayout.GetControlRect(GUILayout.Width(1f));

            if(Event.current.type != EventType.Repaint)
                return;

            rect.yMin -= 2f;
            rect.yMax += 2f;

            Styles.verticalLine.Draw(rect, GUIContent.none, 0);
        }

        private void LoadPublisherInfo() {
            if(publisher != null)
                return;

            loadingData.LoadingData = true;
            AssetStore.PublisherOverview(data => {
                loadingData.LoadingData = false;
                data.AssertError();
                publisher = data.Object;
                loadingData.LoadingData = true;

                AssetStore.GetSalesPeriods(publisher.PublisherID, periods => {
                    loadingData.LoadingData = false;
                    data.AssertError();
                    periodsData = periods.Object;

                    if(periodsData != null && periodsData.periods.Length > 0)
                        SelectedPeriod = periodsData.periods[0];
                });
            });
        }

    }
}