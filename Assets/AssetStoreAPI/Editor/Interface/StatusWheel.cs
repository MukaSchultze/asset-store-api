﻿using UnityEditor;
using UnityEngine;

namespace AssetStoreAPI.Interface {
    public static class StatusWheel {

        private static readonly GUIContent[] content;

        private static int Frame { get { return (int)((EditorApplication.timeSinceStartup * 10d) % 12d); } }

        static StatusWheel() {
            content = new GUIContent[12];

            for(var i = 0; i < 12; i++) {
                content[i] = EditorGUIUtility.IconContent("WaitSpin" + i.ToString("00"));
                content[i] = new GUIContent(content[i]);
            }
        }

        public static void DrawLayout(params GUILayoutOption[] options) {
            GUI.DrawTexture(EditorGUILayout.GetControlRect(false, options), content[Frame].image, ScaleMode.ScaleToFit);
        }

        public static void DrawLayout(string text, params GUILayoutOption[] options) {
            content[Frame].text = " " + text;
            GUILayout.Label(content[Frame], options);
            content[Frame].text = string.Empty;
        }

        public static void DrawLayout(GUIStyle style, params GUILayoutOption[] options) {
            GUILayout.Label(content[Frame], style, options);
        }

        public static void DrawLayout(string text, GUIStyle style, params GUILayoutOption[] options) {
            content[Frame].text = " " + text;
            GUILayout.Label(content[Frame], style, options);
            content[Frame].text = string.Empty;
        }

        public static void Draw(Rect rect) {
            GUI.Label(rect, content[Frame]);
        }

        public static void Draw(Rect rect, string text) {
            content[Frame].text = " " + text;
            GUI.Label(rect, content[Frame]);
            content[Frame].text = string.Empty;
        }

        public static void Draw(Rect rect, GUIStyle style) {
            GUI.Label(rect, content[Frame], style);
        }

        public static void Draw(Rect rect, string text, GUIStyle style) {
            content[Frame].text = " " + text;
            GUI.Label(rect, content[Frame], style);
            content[Frame].text = string.Empty;
        }

    }
}