# Unofficial Asset Store API Wrapper/Tools #

This project uses an undocumented Asset Store API to retrieve data from the publisher dashboard.
It is completely written in C# and was made based on [this repository](https://github.com/LostPolygon/Unity-Publisher-API-PHP/blob/master/AssetStorePublisherClient.class.php).

The project is currently in a "beta" stage, some features were not tested and might not work as intended. I made the project purely testing it with my account data, if you have discrepant data it might misbehave.

The email, password and token are stored locally using EditorPrefs, and are only uploaded to official Unity servers.

Contact me if you want to help or report a bug: [samuelschultze@gmail.com](mailto:samuelschultze@gmail.com).  
My asset store page and my other assets: [http://u3d.as/iYk](http://u3d.as/iYk).  
Also, check out [this](https://github.com/willnode/uas-public-stat#readme) and [this](https://github.com/willnode/uas-private-stat#readme), they are open source web based Asset Store APIs made by [WillNode](http://u3d.as/cco).

**Tested on Unity 2018.1.0b7 and 2017.3.0f2**

### How to use ###

Open the login window by going to the "Asset Store API Tools" menu on the top bar of Unity, then insert your credentials and log in.

![Login Window Screenshot](Assets/Screenshots/Screenshot_4.png)

Now you can use one of the demo windows to test the requests, they're located at the same menu as the login window.

![Demo Window Screenshot](Assets/Screenshots/Screenshot_1.png)

To make your own requests you need to use the methods of the "AssetStoreAPI.AssetStore" class, for example:

![Example code](Assets/Screenshots/Screenshot_6.png)  


### Roadmap ###

* Graphs for free assets and monthly gross;
* Public stats;
* Suggestions are welcome.

### Screenshots ###

![Screenshot](Assets/Screenshots/Screenshot_2.png)  

![Screenshot](Assets/Screenshots/Screenshot_3.png)  

![Screenshot](Assets/Screenshots/Screenshot_5.png)  
